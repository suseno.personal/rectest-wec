<?php
namespace Orm;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class Role extends Eloquent
{
    protected $table = 'role';
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $primaryKey = 'id_role';
    use SoftDeletes;
    
    protected static function boot() {
        parent::boot();
        static::creating(function ($model) {
            $model->created_by = '999';
        });

        static::updating(function ($model) {
            $model->updated_by = '999';
        });
    }
    
}
