<?php
namespace Orm;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Permission extends Eloquent
{
    protected $table = 'user_role';
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $primaryKey = 'id_user_role';   
}
