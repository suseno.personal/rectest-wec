<?php
namespace Orm;

use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Eloquent
{
    protected $table = 'user';
    protected $dateFormat = 'Y-m-d H:i:s';
    protected $primaryKey = 'id_user';
    use SoftDeletes;
    
    protected static function boot() {
        parent::boot();
        static::creating(function ($model) {
            $model->created_by = '999';
        });

        static::updating(function ($model) {
            $model->updated_by = '999';
        });
    }
    
}
