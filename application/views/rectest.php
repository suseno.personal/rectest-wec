<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Selamat datang</title>

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Selamat datang</h1>

	<div id="body">
		<p>
			Halo, setelah kamu sampai pada halaman ini artinya kamu berhasil melakukan instalasi project pada localhost.
			<br>
			Sekarang pastikan terlebih dahulu tidak ada tampilan/warning <b class="text-danger">error</b> pada halaman ini ya.
			<br>
			<br>
			Kemudian dibawah ini telah tersedia tabel data user yang diambil <span style="font-size:18px;">dari database, tabel <b><u class="text-success">user</u></b></span>.
			<br>
			Anda dapat membuka <b><u>controller</u></b> untuk melihat <b><u>source code</u></b> nya
		</p>
		
		<div class="row mt-4">
			<div class="col-md-6">
				<h5>tabel: <b>user</b> dengan menggunakan <b><u class="text-info">Laravel Eloquent</u></b></h5>
				<table class="table">
					<thead>
						<tr>
							<th>No</th>
							<th>Username</th>
							<th>Nama</th>
							<th>Password</th>
							<th>Dibuat pada</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1;foreach ($user as $key => $each): ?>
							<tr>
								<td><?= $i; ?></td>
								<td><?= $each->username ?></td>
								<td><?= $each->nama ?></td>
								<td><?= $each->password ?></td>
								<td><?= $each->created_at ?></td>
							</tr>
						<?php $i++;endforeach ?>
					</tbody>
				</table>
				
				<h5>tabel: <b>user</b> dengan menggunakan <b><u class="text-info">CI Query Builder</u></b></h5>
				<table class="table">
					<thead>
						<tr>
							<th>No</th>
							<th>Username</th>
							<th>Nama</th>
							<th>Password</th>
							<th>Dibuat pada</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1;foreach ($user_ci as $key => $each): ?>
							<tr>
								<td><?= $i; ?></td>
								<td><?= $each->username ?></td>
								<td><?= $each->nama ?></td>
								<td><?= $each->password ?></td>
								<td><?= $each->created_at ?></td>
							</tr>
						<?php $i++;endforeach ?>
					</tbody>
				</table>
				
				
				<h5>Direktory <span class="text-info">ORM / Laravel Eloquent</span> pada project CI</h5>
				<img src="<?= base_url('assets/img/ormdir.jpeg') ?>" width="350px" alt="">
			</div>
		</div>
		
		<hr>
		
		<div class="row mt-4">
			<div class="col-md-6">
				<h3 class="text-warning">Tugas 1</h3>
				
				<div class="ml-4">
					<h5>contoh output yang diharapkan</h5>
					<table class="table">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama Role</th>
								<th>Dibuat pada</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>role_A</td>
								<td>2021-11-01 11:11:11</td>
							</tr>
							<tr>
								<td>2</td>
								<td>role_B</td>
								<td>2021-11-01 11:11:11</td>
							</tr>
						</tbody>
					</table>
				</div>
				
				<ul>
					<li style="list-style: disc;">Tema : Menggunakan <b>CI - Query Builder / Laravel Eloquent</b></li>
					<li style="list-style: disc;">Anda harus membuat tabel <b><u>data role</u></b> dengan data <span style="font-size:18px;">dari database, tabel <b><u class="text-success">role</u></b></span> dengan menggunakan <b><u class="text-info">CI Query Builder</u></b> atau <b><u class="text-info">Laravel Eloquent</u></b> seperti tabel data user diatas</li>
					<li style="list-style: disc;">Anda dapat meletakkan jawaban dengan mengedit tabel yang sudah disediakan dibawah ini.</li>
				</ul>
				
				<div class="ml-4">
					<br>
					<h4 class="text-success">JAWABAN:</h4>
					<h5>tabel: <b>role</b></h5>
					<span>Silahkan letakkan jawaban dengan meng-edit tabel dibawah ini untuk menjawab</span>
					
					
					<!-- START, LETAKKAN JAWABAN 1 DISINI-->
					<table class="table">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama Role</th>
								<th>Dibuat pada</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1;foreach ($role as $key => $row): ?>
							<tr>
								<td><?= $i ?></td>
								<td><?= $row->nama_role ?></td>
								<td><?= $row->created_at ?></td>
							</tr>
							<?php $i++;endforeach ?>
						</tbody>
					</table>
					<!-- END, LETAKKAN JAWABAN 1 DISINI-->
					
				</div>
			</div>
		</div>
		
		<div class="row mt-4">
			<div class="col-md-6">
				<h3 class="text-warning">Tugas 2</h3>
				
				<div class="ml-4">
					<h5>contoh output yang diharapkan</h5>
					<table class="table">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama User</th>
								<th>Nama Role</th>
								<th>Dibuat pada</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Peserta A</td>
								<td>role_A</td>
								<td>2021-11-01 11:11:11</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Peserta A</td>
								<td>role_B</td>
								<td>2021-11-01 11:11:11</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Peserta B</td>
								<td>role_A</td>
								<td>2021-11-01 11:11:11</td>
							</tr>
							<tr>
								<td>4</td>
								<td>Peserta B</td>
								<td>role_B</td>
								<td>2021-11-01 11:11:11</td>
							</tr>
						</tbody>
					</table>
				</div>
				
				<ul>
					<li style="list-style: disc;">Tema : Menggunakan <b>CI Query Builder Join / Eloquent Relationships</b></li>
					<li style="list-style: disc;">Anda harus membuat tabel <b><u>data user role</u></b> dengan data <span style="font-size:18px;">dari database, tabel <b><u class="text-success">user_role</u></b></span> dengan menggunakan <b><u class="text-info">CI Query Builder</u></b> atau <b><u class="text-info">Laravel Eloquent</u></b> seperti tabel data user diatas</li>
					<li style="list-style: disc;">Anda dapat meletakkan jawaban dengan mengedit tabel yang sudah disediakan dibawah ini.</li>
				</ul>
				
				<div class="ml-4">
					<br>
					<h4 class="text-success">JAWABAN:</h4>
					<h5>tabel: <b>user role</b></h5>
					<span>Silahkan letakkan jawaban dengan meng-edit tabel dibawah ini untuk menjawab</span>
					
					<!-- START, LETAKKAN JAWABAN 1 DISINI-->
					<table class="table">
						<thead>
							<tr>
								<th>No</th>
								<th>Nama User</th>
								<th>Nama Role</th>
								<th>Dibuat pada</th>
 							</tr>
						</thead>
						<tbody>
							<?php $i = 1;foreach ($permission as $key => $row): ?>
							<tr>
								<td><?= $i ?></td>
								<td><?= $row->nama ?></td>
								<td><?= $row->nama_role ?></td>
								<td><?= $row->created_at ?></td>
							</tr>
							<?php $i++;endforeach ?>
						</tbody>
					</table>
					<!-- END, LETAKKAN JAWABAN 2 DISINI-->
				</div>
			</div>
		</div>

		<div class="row mt-4">
			<div class="col-md-6">
				<h3 class="text-warning">Tugas 3</h3>
				
				Untuk Tugas Selanjutnya silanhkan membuat <b>CRUD</b> pada tabel User dengan beberapa ketentuan berikut :
				<ul>
					<li style="list-style: decimal;">Menggunakan <b>CI - Query Builder / Laravel Eloquent</b></li>
					<li style="list-style: decimal;">Menggunakan Ajax menjadi nilai tambah</li>
					<li style="list-style: decimal;">Menggunakan <b>Datatables</b></li>
					<li style="list-style: decimal;">Untuk hapus data dan simpan, harus ada konfirmasi lebih dulu </li>
				</ul>
				dengan mekanisme dibawah:
				<ul>
					<li style="list-style: decimal;">Buat <b>controller</b> baru untuk fungsi CRUD yang akan dibuat</li>
					<li style="list-style: decimal;">Buat form dengan view baru atau bisa dengan menggunakan modal</li>
					<li style="list-style: decimal;">Selebihnya silahkan improvisasi sendiri</li>
				</ul>
			</div>
		</div>
		
		<div class="row mt-4">
			<div class="col-md-6">
				<h3 class="text-warning">Tugas 4</h3>
				
				Setelah anda menyelesaikan tugas diatas anda harus mengumpulkannya dengan mekanisme dibawah:
				<ul>
					<li style="list-style: decimal;">Buat git repo (bebas, disarankan gitlab) dengan akses <b>publik</b> berisi seluruh aplikasi yang berisikan jawaban anda</li>
					<li style="list-style: decimal;">Kirimkan link repository ke <a href="https://wa.me/6285641125599">https://wa.me/6285641125599</a></li>
					<li style="list-style: decimal;">Informasi penerimaan akan kita sampaikan via chat whatsapp</li>
				</ul>
				
			</div>
		</div>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
<footer>
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</footer>
</html>
