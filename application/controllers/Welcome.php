<?php
defined('BASEPATH') OR exit('No direct script access allowed');
use Orm\User;
use Orm\Role;
use Orm\Permission;
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Database\Eloquent\Builder;

class Welcome extends CI_Controller {
	
	public function index()
	{
		$data['user']    = User::all();
		$data['user_ci'] = $this->db->get('user')->result();

		$data['role']    = Role::all();
		$data['role_ci'] = $this->db->get('role')->result();

		$data['permission']  = Permission::join('user', 'user_role.id_user', '=', 'user.id_user')
								->join('role', 'user_role.id_role', '=', 'role.id_role')
								->get(['user.nama', 'role.nama_role', 'user_role.created_at']);

		$this->db->select('user.nama, role.nama_role, user_role.created_at');
		$this->db->from('user_role');
		$this->db->join('user', 'user_role.id_user = user.id_user');
		$this->db->join('role', 'user_role.id_role = role.id_role');
		$query = $this->db->get();
		$data['permission_ci'] = $query->result();
		
		$this->load->view('rectest',$data);
	}
}
